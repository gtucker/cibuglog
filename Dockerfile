FROM python:3.7

ENV DEBIAN_FRONTEND noninteractive

COPY . /app

RUN pip install uwsgi
RUN pip install --no-cache-dir -r /app/requirements-dev.txt

WORKDIR /app
CMD uwsgi /app/uwsgi-docker.ini
