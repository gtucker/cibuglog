from django.contrib import admin
from django.forms import ModelForm, ModelMultipleChoiceField
from django.contrib.admin.widgets import FilteredSelectMultiple
from .models import BugTracker, Bug, Component, Build, Test, Machine, RunConfigTag, RunConfig, TestSuite, TestsuiteRun
from .models import TextStatus, TestResult, IssueFilter, IssueFilterAssociated, Issue, KnownFailure, UnknownFailure
from .models import RunFilterStatistic, MachineTag, BugTrackerSLA, BugTrackerAccount, Person, BugComment


class BugTrackerModel(admin.ModelAdmin):
    list_display = ('name', 'public')
    search_fields = ['name']
    readonly_fields = ['polled']


admin.site.register(BugTracker, BugTrackerModel)


class BugModel(admin.ModelAdmin):
    readonly_fields = ['title', 'created', 'updated', 'polled', 'creator', 'product',
                       'assignee', 'component', 'features', 'platforms', 'status', 'priority']
    search_fields = ['bug_id']


admin.site.register(Bug, BugModel)


class BugTrackerSLAModel(admin.ModelAdmin):
    list_display = ('tracker', 'priority', 'SLA')
    search_fields = ('tracker__name', 'priority', 'SLA')


admin.site.register(BugTrackerSLA, BugTrackerSLAModel)


class PersonModel(admin.ModelAdmin):
    list_display = ('full_name', 'email')
    search_fields = ('full_name', 'email')


admin.site.register(Person, PersonModel)


class BugTrackerAccountModel(admin.ModelAdmin):
    list_display = ('tracker', 'person', 'user_id')
    search_fields = ('tracker__name', 'person__email', 'person__last_name', 'person__first_name', 'user_id')


admin.site.register(BugTrackerAccount, BugTrackerAccountModel)


class BugCommentModel(admin.ModelAdmin):
    list_display = ('bug', 'account', 'url', 'created_on')
    search_fields = ('bug', 'account', 'url', 'created_on')


admin.site.register(BugComment, BugCommentModel)


class ComponentModel(admin.ModelAdmin):
    list_display = ('name', 'public')
    search_fields = ['name']


admin.site.register(Component, ComponentModel)


class BuildModel(admin.ModelAdmin):
    readonly_fields = ['added_on']
    list_display = ['name']
    search_fields = ['name', 'version', 'branch', 'repo']


admin.site.register(Build, BuildModel)


class TestModel(admin.ModelAdmin):
    readonly_fields = ['added_on']
    list_display = ('name', 'testsuite', 'public', 'vetted_on')
    search_fields = ['name']


admin.site.register(Test, TestModel)


class MachineTagModel(admin.ModelAdmin):
    readonly_fields = ['added_on']
    list_display = ('name', 'public')
    search_fields = ['name', 'description']


admin.site.register(MachineTag, MachineTagModel)


class MachineModel(admin.ModelAdmin):
    readonly_fields = ['added_on']
    list_display = ('name', 'public', 'vetted_on')
    search_fields = ['name', 'description']


admin.site.register(Machine, MachineModel)


class RunConfigTagModel(admin.ModelAdmin):
    list_display = ('name', 'public')
    search_fields = ['name', 'description']


admin.site.register(RunConfigTag, RunConfigTagModel)


class RunModel(admin.ModelAdmin):
    readonly_fields = ['added_on']
    list_display = ['name']
    search_fields = ['name', 'tags__name']
    filter_horizontal = ['tags', 'builds']


admin.site.register(RunConfig, RunModel)


class TestSuiteModel(admin.ModelAdmin):
    list_display = ('__str__', 'public', 'vetted_on')
    search_fields = ['name', 'description']
    filter_horizontal = ['acceptable_statuses']


admin.site.register(TestSuite, TestSuiteModel)


class TestsuiteRunModel(admin.ModelAdmin):
    list_display = ('runconfig', 'machine', 'run_id')
    search_fields = ['runconfig__name', 'machine__name', 'run_id']


admin.site.register(TestsuiteRun, TestsuiteRunModel)


class TextStatusModel(admin.ModelAdmin):
    list_display = ('__str__', 'testsuite', 'vetted_on')


admin.site.register(TextStatus, TextStatusModel)


class TestResultdModel(admin.ModelAdmin):
    readonly_fields = ['test', 'ts_run', 'status', 'command', 'stdout', 'stderr', 'dmesg']
    list_select_related = ['test', 'ts_run__runconfig', 'ts_run__machine']
    show_full_result_count = False


admin.site.register(TestResult, TestResultdModel)


class IssueFilterForm(ModelForm):
    class Meta:
        model = IssueFilter
        exclude = []
    tests = ModelMultipleChoiceField(queryset=Test.objects.prefetch_related('testsuite').all(),
                                     help_text="The result's machine should be one of these tests "
                                               "(leave empty to ignore tests)",
                                     widget=FilteredSelectMultiple('tests', False))
    statuses = ModelMultipleChoiceField(queryset=TextStatus.objects.prefetch_related('testsuite').all(),
                                        help_text="The result's status should be one of these "
                                                  "(leave empty to ignore results)",
                                        widget=FilteredSelectMultiple('statuses', False))


class IssueFilterModel(admin.ModelAdmin):
    readonly_fields = ['added_on']
    filter_horizontal = ['machines', 'tests', 'statuses', 'tags']
    form = IssueFilterForm

    # Prevent deletion/modification, as we want data from
    def has_delete_permission(self, request, obj=None):
        return False  # pragma: no cover


admin.site.register(IssueFilter, IssueFilterModel)


class IssueFilterAssociatedModel(admin.ModelAdmin):
    readonly_fields = ['added_on']
    raw_id_fields = ['filter', 'issue']
    list_select_related = ['filter', 'issue']
    show_full_result_count = False


admin.site.register(IssueFilterAssociated, IssueFilterAssociatedModel)


class IssueModel(admin.ModelAdmin):
    filter_horizontal = ['filters']
    readonly_fields = ['added_on']


admin.site.register(Issue, IssueModel)


class KnownFailureModel(admin.ModelAdmin):
    raw_id_fields = ['result', 'matched_ifa']
    list_select_related = ['result__test', 'result__ts_run__runconfig', 'result__ts_run__machine', "result__status"]
    show_full_result_count = False


admin.site.register(KnownFailure, KnownFailureModel)


class UnknownFailureModel(admin.ModelAdmin):
    raw_id_fields = ['result']
    list_select_related = ['result__test', 'result__ts_run__runconfig', 'result__ts_run__machine', 'result__status']
    show_full_result_count = False


admin.site.register(UnknownFailure, UnknownFailureModel)


class RunFilterStatisticModel(admin.ModelAdmin):
    list_display = ('__str__', 'covered_count', 'matched_count')
    raw_id_fields = ['filter', 'runconfig']
    list_select_related = ['runconfig', 'filter']


admin.site.register(RunFilterStatistic, RunFilterStatisticModel)
