from datetime import timedelta

from django.conf import settings
from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from CIResults.tests.test_views import ViewMixin, UserFiltrableViewMixin
from CIResults.models import BugTracker, Bug


# HACK: Massively speed up the login primitive. We don't care about security in tests
settings.PASSWORD_HASHERS = ('django.contrib.auth.hashers.MD5PasswordHasher', )


class ApiMetricsTests(TestCase):
    # TODO: Re-visit this test when we have a good fixture!
    fixtures = ['CIResults/fixtures/RunConfigResults_commit_to_db']

    def test_get(self):
        response = self.client.get("/api/metrics")
        self.assertEqual(response.status_code, 200)
        self.assertContains(response,
                            '{"issues": 0, "machine": 1, "suppressed_tests": 1, '
                            '"suppressed_machines": 1, "tests": 1, "unknown_failures": 0, "version": 1}')


class MetricsOverviewTests(ViewMixin, TestCase):
    private_view = "CIResults-metrics-overview"
    public_view = "CIResults-metrics-overview-public"


class MetricsBugsTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-metrics-bugs"
    public_view = "CIResults-metrics-bugs-public"
    query = "status = 'open'"


class MetricsOpenBugsTests(UserFiltrableViewMixin, TestCase):
    fixtures = ['CIResults/fixtures/bugs']

    # TODO: set the current time to a fixed one, add more comments

    private_view = "CIResults-metrics-open-bugs"
    public_view = "CIResults-metrics-open-bugs-public"
    query = "product = 'test'"


class MetricsPassrateTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-metrics-passrate"
    public_view = "CIResults-metrics-passrate-public"
    query = "test_name = 'test'"


class MetricsRunTimeTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-metrics-runtime"
    public_view = "CIResults-metrics-runtime-public"
    query = "machine_name = 'machine'"


class bug_flag_for_updateTests(TestCase):
    def setUp(self):
        tracker = BugTracker.objects.create(name="BugTracker", public=True)
        self.bug = Bug.objects.create(tracker=tracker, bug_id="BUG_1")

    def test_not_being_updated(self):
        response = self.client.post(reverse("CIResults-bug-flag-for-update", kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 200)

        delta = timezone.now() - Bug.objects.get(pk=self.bug.id).flagged_as_update_pending_on
        self.assertLess(delta.total_seconds(), 1)

    def test_being_updated(self):
        self.bug.flagged_as_update_pending_on = timezone.now() - timedelta(seconds=10)
        self.bug.save()

        response = self.client.post(reverse("CIResults-bug-flag-for-update", kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 409)

        delta = timezone.now() - Bug.objects.get(pk=self.bug.id).flagged_as_update_pending_on
        self.assertGreater(delta.total_seconds(), 1)
