from django.core import mail
from django.test import TestCase

from CIResults.email import Email


class EmailTests(TestCase):
    def test_send(self):
        with self.settings(EMAIL_ADDRESS='hello@me.com'):
            email = Email("my subject", 'the wonderful\nmessage',
                          ['one@domain.tld', 'two@domain.tld', 'three@domain.tld'])
            email.send()

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, email.to)
        self.assertEqual(mail.outbox[0].from_email, 'hello@me.com')
        self.assertEqual(mail.outbox[0].subject, email.subject)
        self.assertEqual(mail.outbox[0].body, email.message)
