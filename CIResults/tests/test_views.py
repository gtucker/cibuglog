from unittest.mock import patch, MagicMock
from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from CIResults.models import Test, Machine, RunConfigTag, TestSuite, Build
from CIResults.models import TextStatus, IssueFilter, Issue
from CIResults.models import RunConfig, TestsuiteRun, TestResult, Component
from CIResults.views import ResultsCompareView

from datetime import timedelta


# HACK: Massively speed up the login primitive. We don't care about security in tests
settings.PASSWORD_HASHERS = ('django.contrib.auth.hashers.MD5PasswordHasher', )


class ViewMixin:
    view_kwargs = {}

    @property
    def private_url(self):
        if not hasattr(self, 'private_view'):
            self.skipTest("No private view set for the class")  # pragma: no cover
        return reverse(self.private_view, kwargs=self.view_kwargs)

    @property
    def public_url(self):
        if not hasattr(self, 'public_view'):
            self.skipTest("No public view set for the class")  # pragma: no cover
        return reverse(self.public_view)

    def test_get_private(self):
        response = self.client.get(self.private_url)
        self.assertEqual(response.status_code, 200)

    def test_get_public(self):
        # Url checks
        self.assertTrue(self.public_url.endswith('.html'))

        response = self.client.get(self.public_url)
        self.assertEqual(response.status_code, 200)

        # Check that we have no history links
        self.assertNotContains(response, "/history")


class UserFiltrableViewMixin(ViewMixin):
    def test_invalid_query(self):
        response = self.client.get(self.private_url + "?query=djzkhjkf")
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Filtering error")

    def test_valid_query(self):
        response = self.client.get(self.private_url + "?query=" + self.query)
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, "Filtering error")


class IndexTests(ViewMixin, TestCase):
    private_view = "CIResults-index"
    public_view = "CIResults-index-public"


class IssueListTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-issues-list"
    public_view = "CIResults-issues-list-public"
    query = "filter_description = 'desc'"


class TestTests(ViewMixin, TestCase):
    private_view = "CIResults-tests"
    public_view = "CIResults-tests-public"


class TestMassRenameTests(ViewMixin, TestCase):
    private_view = "CIResults-tests-massrename"


class MachineTests(ViewMixin, TestCase):
    private_view = "CIResults-machines"
    public_view = "CIResults-machines-public"


class TestResultListViewTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-results"
    query = "test_name = 'test'"


class KnownFailureListViewTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-knownfailures"
    query = "test_name = 'test'"


class ResultsCompareTests(ViewMixin, TestCase):
    private_view = "CIResults-compare"

    def test_urlify(self):
        view = ResultsCompareView()
        string = "Hello http://gitlab.freedesktop.org, https://x.org"
        self.assertEqual(view.urlify(string),
                         "Hello <http://gitlab.freedesktop.org>, <https://x.org>")

    def test_invalid_runconfig(self):
        response = self.client.get(self.private_url + "?from=RUNCONFIG1&to=RUNCONFIG2")
        self.assertEqual(response.status_code, 200)

    @patch('CIResults.models.RunConfig.objects.filter')
    def test_valid_runconfig(self, filter_mocked):
        filter_mocked.return_value.first.return_value.compare.return_value.text = "Test"
        response = self.client.get(self.private_url + "?from=RUNCONFIG1&to=RUNCONFIG2")
        self.assertEqual(response.status_code, 200)


class MetricsOverviewTests(ViewMixin, TestCase):
    private_view = "CIResults-metrics-overview"
    public_view = "CIResults-metrics-overview-public"


class MetricsBugsTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-metrics-bugs"
    public_view = "CIResults-metrics-bugs-public"
    query = "status = 'open'"


class MetricsOpenBugsTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-metrics-open-bugs"
    public_view = "CIResults-metrics-open-bugs-public"
    query = "product = 'test'"


class MetricsPassrateTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-metrics-passrate"
    public_view = "CIResults-metrics-passrate-public"
    query = "test_name = 'test'"


class MetricsRunTimeTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-metrics-runtime"
    public_view = "CIResults-metrics-runtime-public"
    query = "machine_name = 'machine'"


class MachineMassVettingTests(TestCase):
    def setUp(self):
        self.url = reverse('CIResults-machine-mass-vetting')

    def test_get_request(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 405)

    @patch('CIResults.models.Machine.objects.filter', return_value=[MagicMock(name='machine 1'),
                                                                    MagicMock(name='machine 2'),
                                                                    MagicMock(name='machine 3')])
    def test_normal_query(self, machine_filter_mocked):
        response = self.client.post(self.url, {'1': 'on', '2': 'on', '3': 'on', 'gfdgdfg': 'invalid'})
        self.assertEqual(response.status_code, 302)

        # Check that all the machines were requested from the DB
        machine_filter_mocked.assert_called_once_with(pk__in=set([1, 2, 3]))

        # Check that all machine returned were vetted
        for machine in machine_filter_mocked.return_value:
            machine.vet.assert_called_once_with()


class IssueDetailTests(ViewMixin, TestCase):
    private_view = "CIResults-issue-detail"
    view_kwargs = {'pk': 1}

    def setUp(self):
        Issue.objects.create(filer='me@me.org', expected=True)


class IFADetailTests(ViewMixin, TestCase):
    private_view = "CIResults-ifa-detail"
    view_kwargs = {'pk': 1}

    def setUp(self):
        issue = Issue.objects.create(filer='me@me.org', expected=True)
        f = IssueFilter.objects.create(description='filter')
        issue.set_filters([f])


class MachineDetailTests(ViewMixin, TestCase):
    private_view = "CIResults-machine-detail"
    view_kwargs = {'pk': 1}

    def setUp(self):
        Machine.objects.create(name='Machine1', public=True)


class TestSuiteDetailTests(ViewMixin, TestCase):
    private_view = "CIResults-testsuite-detail"
    view_kwargs = {'pk': 1}

    def setUp(self):
        TestSuite.objects.create(name='TestSuite', public=True)


class TestDetailTests(ViewMixin, TestCase):
    private_view = "CIResults-test-detail"
    view_kwargs = {'pk': 1}

    def setUp(self):
        ts = TestSuite.objects.create(name='TestSuite', public=True)
        Test.objects.create(name='test1', testsuite=ts, public=True)


class TextStatusDetailTests(ViewMixin, TestCase):
    private_view = "CIResults-textstatus-detail"
    view_kwargs = {'pk': 1}

    def setUp(self):
        ts = TestSuite.objects.create(name='TestSuite', public=True)
        TextStatus.objects.create(name='status1', testsuite=ts)


class TestResultDetailTests(ViewMixin, TestCase):
    private_view = "CIResults-testresult-detail"
    view_kwargs = {'pk': 1}

    def setUp(self):
        ts = TestSuite.objects.create(name='TestSuite', public=True)
        machine = Machine.objects.create(name='Machine1', public=True)
        runconfig = RunConfig.objects.create(name='runconfig', temporary=True)
        ts_run = TestsuiteRun.objects.create(testsuite=ts, machine=machine, runconfig=runconfig,
                                             run_id=0, start=timezone.now(), duration=timedelta(hours=3))

        test = Test.objects.create(name='test1', testsuite=ts, public=True)
        status = TextStatus.objects.create(name='status1', testsuite=ts)
        TestResult.objects.create(test=test, status=status, ts_run=ts_run,
                                  start=timezone.now(), duration=timedelta(seconds=5))


class RunConfigDetailTests(ViewMixin, TestCase):
    private_view = "CIResults-runcfg-detail"
    view_kwargs = {'pk': 1}

    def setUp(self):
        RunConfig.objects.create(name='runconfig', temporary=True)


class RunConfigTagDetailTests(ViewMixin, TestCase):
    private_view = "CIResults-runcfgtag-detail"
    view_kwargs = {'pk': 1}

    def setUp(self):
        RunConfigTag.objects.create(name='tag', public=True)


class BuildDetailTests(ViewMixin, TestCase):
    private_view = "CIResults-build-detail"
    view_kwargs = {'pk': 1}

    def setUp(self):
        component = Component.objects.create(name='component', public=True)
        Build.objects.create(name='build1', component=component)


class ComponentDetailTests(ViewMixin, TestCase):
    private_view = "CIResults-component-detail"
    view_kwargs = {'pk': 1}

    def setUp(self):
        Component.objects.create(name='runconfig', public=True)
