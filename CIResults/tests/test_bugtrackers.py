from unittest.mock import patch, MagicMock, PropertyMock
from django.test import TestCase, TransactionTestCase
from dateutil import parser as dateparser

from CIResults.models import BugTracker, Bug, Person, BugTrackerAccount, BugComment
from CIResults.bugtrackers import BugTrackerCommon, Bugzilla, Untracked, Jira, GitLab

from collections import namedtuple
from jira.exceptions import JIRAError
import urllib.parse
import xmlrpc.client
import requests
import datetime
import pytz


class BugTrackerCommonTests(TestCase):
    @patch('CIResults.models.BugTrackerAccount.objects.filter', return_value=[BugTrackerAccount(user_id="1"),
                                                                              BugTrackerAccount(user_id="2"),
                                                                              BugTrackerAccount(user_id="3")])
    def test_account_cached(self, filter_mocked):
        db_tracker = BugTracker(name="Tracker1", public=True)
        common = BugTrackerCommon(db_tracker)
        accounts = common.accounts_cached

        filter_mocked.assert_called_with(tracker=db_tracker)
        self.assertEqual(accounts, {"1": filter_mocked.return_value[0],
                                    "2": filter_mocked.return_value[1],
                                    "3": filter_mocked.return_value[2]})

    # Check that the corresponding account is returned if it exists
    def test_find_or_create_account__existing(self):
        user_id = "my id"

        db_tracker = BugTracker(name="Tracker1", public=True)
        common = BugTrackerCommon(db_tracker)
        common.accounts_cached = {user_id: MagicMock()}

        account = common.find_or_create_account(user_id)
        self.assertEqual(account, common.accounts_cached[user_id])
        self.assertEqual(Person.objects.all().count(), 0)
        self.assertEqual(BugTrackerAccount.objects.all().count(), 0)

    # Check that a new account is created when it does not exist yet, then
    # that subsequent changes get registered
    def test_find_or_create_account(self):
        user_id = "my id"
        name = "John Doe"
        email = "me@email.com"

        db_tracker = BugTracker.objects.create(name="Tracker1", public=True)
        common = BugTrackerCommon(db_tracker)
        account = common.find_or_create_account(user_id, email=email, full_name=name)

        self.assertEqual(account.tracker, db_tracker)
        self.assertEqual(account.user_id, user_id)
        self.assertEqual(account.is_developer, False)
        self.assertEqual(account.person.full_name, name)
        self.assertEqual(account.person.email, email)

        name2 = "John Doe 2"
        common.find_or_create_account(user_id, email=email, full_name=name2)
        account = BugTrackerAccount.objects.get(user_id=user_id)
        self.assertEqual(account.person.full_name, name2)
        self.assertEqual(account.person.email, email)

        email2 = "me2@email.com"
        common.find_or_create_account(user_id, email=email2, full_name=name)
        account = BugTrackerAccount.objects.get(user_id=user_id)
        self.assertEqual(account.person.full_name, name)
        self.assertEqual(account.person.email, email2)

    def test_create_bug(self):
        db_tracker = BugTracker.objects.create(name="Tracker1", project="FOO", public=True)
        common = BugTrackerCommon(db_tracker)
        fields = {'title': "Kwyjibo",
                  'status': "D'oh",
                  'description': "A big, dumb, balding North American ape.",
                  'product': "The Simpsons",
                  'platforms': "TV",
                  'priority': "High",
                  'component': "Homer"}

        bug = Bug.objects.create(tracker=db_tracker, **fields)
        common.create_bug_from_json = MagicMock()
        common.create_bug(bug)
        out_fields = common.create_bug_from_json.call_args[0][0]
        for field in fields:
            self.assertEqual(fields[field], out_fields[field])

    def test_create_bug_existing(self):
        db_tracker = BugTracker.objects.create(name="Tracker1", project="FOO", public=True)
        common = BugTrackerCommon(db_tracker)
        fields = {'title': "Kwyjibo",
                  'status': "D'oh",
                  'description': "A big, dumb, balding North American ape.",
                  'product': "The Simpsons",
                  'platforms': "TV",
                  'priority': "High",
                  'component': "Homer"}

        bug = Bug.objects.create(tracker=db_tracker, bug_id=10, **fields)
        common.create_bug_from_json = MagicMock()
        with self.assertRaises(ValueError):
            common.create_bug(bug)

    def test_create_bug_no_project(self):
        db_tracker = BugTracker.objects.create(name="Tracker1", public=True)
        common = BugTrackerCommon(db_tracker)
        fields = {'title': "Kwyjibo",
                  'status': "D'oh",
                  'description': "A big, dumb, balding North American ape.",
                  'product': "The Simpsons",
                  'platforms': "TV",
                  'priority': "High",
                  'component': "Homer"}

        bug = Bug.objects.create(tracker=db_tracker, **fields)
        common.create_bug_from_json = MagicMock()
        with self.assertRaises(ValueError):
            common.create_bug(bug)


class RequestsGetMock():
    PRIVATE_TOKEN = "qwerttyzxcfdsapjdpfa"
    BUG_ID = 2
    BUG_CREATED_AT = '2018-10-04T11:20:48.531Z'
    BUG_UPDATED_AT = '2018-11-28T13:24:13.325Z'
    CREATOR_NAME = 'Creator Name'
    ASSIGNEE_NAME = 'Assignee Name'
    NOTE_ONE_ID = 83161
    NOTE_ONE_CREATOR_NAME = 'Note Creator'
    NOTE_ONE_CREATED_AT = '2018-11-28T13:24:13.290Z'
    BUG_TITLE = 'super bug title'
    BUG_STATUS = 'opened'
    BUG_DESCRIPTION = 'the cake is a lie'
    RESPONSES = {
        'https://gitlab.freedesktop.org/api/v4/projects/230/issues/2':
        {
            'id': 4674,
            'iid': BUG_ID,
            'project_id': 230,
            'title': BUG_TITLE,
            'description': BUG_DESCRIPTION,
            'state': BUG_STATUS,
            'created_at': BUG_CREATED_AT,
            'updated_at': BUG_UPDATED_AT,
            'closed_at': None,
            'closed_by': None,
            'author': {'id': 1127, 'name': CREATOR_NAME},
            'assignee': {'id': 1128, 'name': ASSIGNEE_NAME},
            'web_url': 'https://gitlab.freedesktop.org/patchwork-fdo/patchwork-fdo/issues/2'
            },
        'https://gitlab.freedesktop.org/api/v4/projects/230/issues/2/notes':
        [
            {
                'id': NOTE_ONE_ID,
                'author': {'id': 1129, 'name': NOTE_ONE_CREATOR_NAME},
                'created_at': NOTE_ONE_CREATED_AT
                },
            {
                'id': 41381,
                'author': {'id': 1127, 'name': CREATOR_NAME},
                'created_at': '2018-10-04T12:35:03.299Z'
                }
            ],
        'https://gitlab.freedesktop.org/api/v4/projects/230/issues/':
        [
            {'id': 4675, 'iid': 3, 'project_id': 230},
            {'id': 4674, 'iid': 2, 'project_id': 230},
            {'id': 4673, 'iid': 1, 'project_id': 230}
            ]
        }

    def __init__(self, url, **kwargs):
        self.url = url

        if url not in self.RESPONSES.keys():
            raise ValueError("unknown URL: {}".format(url))  # pragma: no cover

        if kwargs['headers']['PRIVATE-TOKEN'] != self.PRIVATE_TOKEN:
            raise ValueError("GitLab needs PRIVATE-TOKEN for querying API")  # pragma: no cover

    def raise_for_status(self):
        pass

    def json(self):
        return self.RESPONSES[self.url]


class BugTrackerGitLabTests(TransactionTestCase):
    def setUp(self):
        url = "https://gitlab.freedesktop.org"
        bug_base_url = "https://gitlab.freedesktop.org/patchwork-fdo/patchwork-fdo/issues/"

        self.db_tracker = BugTracker.objects.create(tracker_type="gitlab", public=True,
                                                    project="230",
                                                    password=RequestsGetMock.PRIVATE_TOKEN,
                                                    url=url, bug_base_url=bug_base_url)

        self.bug = Bug.objects.create(tracker=self.db_tracker, bug_id=str(RequestsGetMock.BUG_ID))
        self.gitlab = GitLab(self.db_tracker)

    @patch('requests.get', RequestsGetMock)
    def testPolledBugShouldSaveJustFine(self):
        self.gitlab.poll(self.bug)
        self.bug.save()

    @patch('requests.get', RequestsGetMock)
    def testPollingBugShouldPopulateFields(self):
        self.gitlab.poll(self.bug)
        self.assertEqual(self.bug.title, RequestsGetMock.BUG_TITLE)
        self.assertEqual(self.bug.status, RequestsGetMock.BUG_STATUS)
        self.assertEqual(self.bug.assignee.person.full_name, RequestsGetMock.ASSIGNEE_NAME)
        self.assertEqual(self.bug.creator.person.full_name, RequestsGetMock.CREATOR_NAME)
        self.assertEqual(self.bug.created, RequestsGetMock.BUG_CREATED_AT)
        self.assertEqual(self.bug.updated, RequestsGetMock.BUG_UPDATED_AT)
        self.assertEqual(self.bug.description, RequestsGetMock.BUG_DESCRIPTION)

    @patch('requests.get', RequestsGetMock)
    def testPollingBugShouldFetchComments(self):
        self.gitlab.poll(self.bug)

        comments = BugComment.objects.filter(bug=self.bug)
        self.assertEquals(comments.count(), 2)

    @patch('requests.get', RequestsGetMock)
    def testNoteShouldBePopulatedCorrectly(self):
        self.gitlab.poll(self.bug)

        comment = BugComment.objects.get(bug=self.bug, comment_id=RequestsGetMock.NOTE_ONE_ID)
        self.assertEqual(comment.account.person.full_name, RequestsGetMock.NOTE_ONE_CREATOR_NAME)
        self.assertEqual(comment.created_on, dateparser.parse(RequestsGetMock.NOTE_ONE_CREATED_AT))
        self.assertTrue("#note_{}".format(comment.comment_id) in comment.url)
        self.assertTrue(self.db_tracker.bug_base_url in comment.url)

    @patch('requests.get', RequestsGetMock)
    def testPollingBugTwiceShouldNotDuplicateComments(self):
        self.gitlab.poll(self.bug)
        self.bug.comments_polled = None  # force polling
        self.gitlab.poll(self.bug)

        comments = BugComment.objects.filter(bug=self.bug)
        self.assertEquals(comments.count(), 2)

    @patch('requests.get', RequestsGetMock)
    def testSearchAllBugIds(self):
        # FIXME: test querying more thoroughly
        all_bugs = self.gitlab.search_bugs_ids(created_since=datetime.datetime.fromtimestamp(1000),
                                               status=['status1', 'status2'])
        self.assertEqual(all_bugs, set(['1', '2', '3']))

    def test_open_statuses(self):
        self.assertEqual(GitLab.open_statuses, ['opened'])

    @patch('requests.post')
    def testAddComment(self, post_mock):
        comment = "Hello world!"
        self.gitlab.add_comment(Bug(tracker=self.db_tracker, bug_id=RequestsGetMock.BUG_ID), comment)

        # Check that the call was what was expected
        args, kwargs = post_mock.call_args_list[0]
        self.assertEqual(args[0], 'https://gitlab.freedesktop.org/api/v4/projects/230/issues/2/notes')
        self.assertEqual(kwargs['headers'], {'PRIVATE-TOKEN': RequestsGetMock.PRIVATE_TOKEN})
        self.assertEqual(kwargs['params'], {'body': comment})

    @patch('requests.post')
    def test_create_bug_from_json(self, post_mock):
        summary = "summary"
        description = "description"
        test_id = 5678
        json_bug = {'title': summary,
                    'description': description,
                    'labels': "Bug",
                    'state': "opened"}

        post_mock.return_value.raise_for_status.return_value = None
        post_mock.return_value.json.return_value = {"iid": test_id}

        id = self.gitlab.create_bug_from_json(json_bug)
        self.assertEqual(id, test_id)

        args, kwargs = post_mock.call_args_list[0]
        request = kwargs['params']
        for field in json_bug:
            self.assertEqual(request[field], json_bug[field])

    @patch('requests.post')
    def test_create_bug_from_json_no_labels(self, post_mock):
        summary = "summary"
        description = "description"
        test_id = 5678
        json_bug = {'title': summary,
                    'description': description}

        post_mock.return_value.raise_for_status.return_value = None
        post_mock.return_value.json.return_value = {"iid": test_id}

        id = self.gitlab.create_bug_from_json(json_bug)
        self.assertEqual(id, test_id)

        args, kwargs = post_mock.call_args_list[0]
        expected_request = {'title': summary,
                            'description': description,
                            'labels': "Bug"}
        request = kwargs['params']
        for field in expected_request:
            self.assertEqual(request[field], expected_request[field], field)

    @patch('requests.post')
    def test_create_bug_from_json_with_status(self, post_mock):
        summary = "summary"
        description = "description"
        test_id = 5678
        json_bug = {'title': summary,
                    'description': description,
                    'labels': "Bug",
                    'status': "opened"}

        post_mock.return_value.raise_for_status.return_value = None
        post_mock.return_value.json.return_value = {"iid": test_id}

        id = self.gitlab.create_bug_from_json(json_bug)
        self.assertEqual(id, test_id)

        args, kwargs = post_mock.call_args_list[0]
        expected_request = {'title': summary,
                            'description': description,
                            'labels': "Bug",
                            'state': "opened"}
        request = kwargs['params']
        for field in expected_request:
            self.assertEqual(request[field], expected_request[field], field)

    @patch('requests.post')
    def test_create_malformed_bug(self, post_mock):
        summary = "summary"
        description = "description"
        json_bug = {'title': summary,
                    'description': description,
                    'labels': "Bug"}

        post_mock.side_effect = requests.HTTPError

        with self.assertRaises(ValueError):
            self.gitlab.create_bug_from_json(json_bug)


class BugzillaProxyMock:
    URL = "https://bugzilla.instance.org"

    # User.login
    LOGIN = "userlogin"
    PASSWORD = "password"
    TOKEN_ID = '12345'
    TOKEN = '12345-kZ5CYMeQGH'

    # Bug.add_comment
    BUG_ID = 1234
    COMMENT = 'my comment'

    # Bugzilla.create_bug
    NEW_BUG_ID = 5678
    PRODUCT = "TEST_PRODUCT"
    COMPONENT = "TEST/COMPONENT/WITH/SLASHES"
    SUMMARY = "TEST_SUMMARY"
    DESCRIPTION = "TEST_DESCRIPTION"

    PROJECT = "{}/{}".format(PRODUCT, COMPONENT)

    CREATE_REQUEST = {
                       'token': TOKEN,
                       'product': PRODUCT,
                       'component': COMPONENT,
                       'summary': SUMMARY,
                       'description': DESCRIPTION
                      }

    class _User:
        def login(self, params):
            if params.get('login') != BugzillaProxyMock.LOGIN:
                raise ValueError('Incorrect or missing login')  # pragma: no cover
            if params.get('password') != BugzillaProxyMock.PASSWORD:
                raise ValueError('Incorrect or missing password')  # pragma: no cover
            return {'id': BugzillaProxyMock.TOKEN_ID, 'token': BugzillaProxyMock.TOKEN}

    class _Bug:
        def add_comment(self, params):
            if params.get('id') != BugzillaProxyMock.BUG_ID:
                raise ValueError('Incorrect or missing bug id')  # pragma: no cover
            if params.get('token') != BugzillaProxyMock.TOKEN:
                raise ValueError('Incorrect or missing token')  # pragma: no cover
            if params.get('comment') != BugzillaProxyMock.COMMENT:
                raise ValueError('Incorrect or missing comment')  # pragma: no cover
            return {'id': 766846}

    def __init__(self, url, use_builtin_types=False):
        if url != self.URL + "/xmlrpc.cgi":
            raise ValueError('invalid xmlrpc url')  # pragma: no cover

        if not use_builtin_types:
            raise ValueError('use_builtin_types is not True')  # pragma: no cover

    User = _User()
    Bug = _Bug()


class BugTrackerBugzillaTests(TestCase):
    def test__get_user_id(self):
        self.assertEqual(Bugzilla._get_user_id({'creator_detail': {'email': 'me@email.com',
                                                                   'name': 'John Doe'}},
                                               'creator'), 'me@email.com')

        self.assertEqual(Bugzilla._get_user_id({'creator_detail': {'name': 'John Doe'}},
                                               'creator'), 'John Doe')

        self.assertRaisesMessage(ValueError,
                                 'Cannot find a good identifier for the user of the bug 1234',
                                 Bugzilla._get_user_id, {'id': '1234'}, 'creator')

    def test_list_to_str(self):
        self.assertEqual(Bugzilla._list_to_str(['one', 'two', 'three']), 'one,two,three')
        self.assertEqual(Bugzilla._list_to_str('one'), 'one')

    def test_bug_id_parser(self):
        self.assertEqual(Bugzilla._bug_id_parser(MagicMock(spec=Bug, bug_id='1234')), 1234)
        self.assertRaisesMessage(ValueError, "Bugzilla's IDs should be integers (fdo#1234)",
                                 Bugzilla._bug_id_parser, MagicMock(spec=Bug, bug_id='fdo#1234'))

    @patch('xmlrpc.client.ServerProxy')
    def test_poll__with_emails(self, ServerProxy_mock):
        ServerProxy_mock.return_value.Bug.get.return_value = {
            "bugs": [
                {
                    "summary": "summary",
                    "status": "status",
                    "is_open": False,
                    "resolution": "resolution",
                    "creation_time": datetime.datetime.fromtimestamp(0),
                    "last_change_time": datetime.datetime.fromtimestamp(5),
                    "creator_detail": {"real_name": "creator", "email": "creator@me.de"},
                    "assigned_to_detail": {"real_name": "assignee", "email": "assignee@me.de"},
                    "product": "product",
                    "component": "component",
                    "features": ["feature1", "feature2"],
                    "platforms": ["platform1", "platform2"],
                    "priority": "high",
                }
            ]
        }

        description = "Have you tried turning it off and on again?"
        ServerProxy_mock.return_value.Bug.comments.return_value = {
            "bugs": {
                "1": {
                    "comments": [
                        {
                            "text": description,
                            "creator": "Roy Trenneman",
                            "id": 100,
                            "count": 0,
                            "time": datetime.datetime.fromtimestamp(0),
                            "creation_time": datetime.datetime.fromtimestamp(0)
                        }
                    ]
                }
            }
        }

        ServerProxy_mock.return_value.Bug.history.return_value = {
            "bugs": [
                {'history': [
                    {'when': datetime.datetime.fromtimestamp(1), 'who': 'someone@toto.de',
                     'changes': [{'field_name': 'status', 'removed': 'NEW', 'added': 'RESOLVED'},
                                 {'field_name': 'resolution', 'removed': '', 'added': 'FIXED'}]
                     },
                    {'who': 'someone@toto.de', 'when': datetime.datetime.fromtimestamp(2),
                     'changes': [
                        {'field_name': 'status', 'added': 'NEW', 'removed': 'RESOLVED'},
                        {'field_name': 'resolution', 'added': '', 'removed': 'FIXED'}],
                     },
                    {'when': datetime.datetime.fromtimestamp(3), 'who': 'someone@toto.de',
                     'changes': [
                        {'field_name': 'status', 'added': 'RESOLVED', 'removed': 'NEW'},
                        {'field_name': 'resolution', 'removed': '', 'added': 'FIXED'}],
                     },
                    {'when': datetime.datetime.fromtimestamp(4), 'who': 'someone@toto.de',
                     'changes': [{'field_name': 'status', 'added': 'CLOSED', 'removed': 'RESOLVED'}],
                     },
                ]}
            ]
        }

        bug = MagicMock(spec=Bug, closed=None, description=None)
        tracker = BugTracker.objects.create(tracker_type="bugzilla", features_field="features",
                                            platforms_field="platforms", public=True)

        with patch.object(BugComment.objects, "create"):
            Bugzilla(tracker).poll(bug)

        self.assertEqual(bug.title, "summary")
        self.assertEqual(bug.created, datetime.datetime.fromtimestamp(0, tz=pytz.utc))
        self.assertEqual(bug.updated, datetime.datetime.fromtimestamp(5, tz=pytz.utc))
        self.assertEqual(bug.closed, datetime.datetime.fromtimestamp(3, tz=pytz.utc))
        self.assertEqual(bug.creator.person.full_name, "creator")
        self.assertEqual(bug.creator.person.email, "creator@me.de")
        self.assertEqual(bug.assignee.person.full_name, "assignee")
        self.assertEqual(bug.assignee.person.email, "assignee@me.de")
        self.assertEqual(bug.product, "product")
        self.assertEqual(bug.component, "component")
        self.assertEqual(bug.features, "feature1,feature2")
        self.assertEqual(bug.platforms, "platform1,platform2")
        self.assertEqual(bug.status, "status/resolution")
        self.assertEqual(bug.priority, "high")
        self.assertEqual(bug.description, description)
        bug.save.assert_not_called()

    @patch('xmlrpc.client.ServerProxy')
    def test_poll__no_emails(self, ServerProxy_mock):
        ServerProxy_mock.return_value.Bug.get.return_value = {
            "bugs": [
                {
                    "summary": "summary",
                    "status": "status",
                    "is_open": True,
                    "resolution": "resolution",
                    "creation_time": datetime.datetime.fromtimestamp(0),
                    "last_change_time": datetime.datetime.fromtimestamp(1),
                    "creator_detail": {"real_name": "creator", "name": "creator"},
                    "assigned_to_detail": {"real_name": "assignee", "name": "assignee"},
                    "product": "product",
                    "component": "component",
                    "features": ["feature1", "feature2"],
                    "platforms": ["platform1", "platform2"],
                    "priority": "high",
                }
            ]
        }

        description = "Have you tried turning it off and on again?"
        ServerProxy_mock.return_value.Bug.comments.return_value = {
            "bugs": {
                "1": {
                    "comments": [
                        {
                            "text": description,
                            "creator": "Roy Trenneman",
                            "id": 100,
                            "count": 0,
                            "time": datetime.datetime.fromtimestamp(0),
                            "creation_time": datetime.datetime.fromtimestamp(0)
                        }
                    ]
                }
            }
        }

        bug = MagicMock(spec=Bug)
        tracker = BugTracker.objects.create(tracker_type="bugzilla", features_field="features",
                                            platforms_field="platforms", public=True)

        with patch.object(BugComment.objects, "create"):
            Bugzilla(tracker).poll(bug)

        self.assertEqual(bug.creator.person.full_name, "creator")
        self.assertEqual(bug.creator.person.email, None)
        self.assertEqual(bug.assignee.person.full_name, "assignee")
        self.assertEqual(bug.assignee.person.email, None)
        bug.save.assert_not_called()

    @patch('xmlrpc.client.ServerProxy')
    def test_poll_invalid_bug(self, ServerProxy_mock):
        ServerProxy_mock.return_value.Bug.get.return_value = {
            "bugs": []
        }

        tracker = BugTracker.objects.create(tracker_type="bugzilla", name='my tracker', public=True)
        bug = MagicMock(spec=Bug, bug_id='1234', tracker=tracker)
        self.assertRaisesMessage(ValueError, "Could not find the bug ID 1234 on my tracker",
                                 Bugzilla(tracker).poll, bug)

        bug.save.assert_not_called()

    @patch('xmlrpc.client.ServerProxy')
    def test_poll_wrong_comment_count(self, ServerProxy_mock):
        ServerProxy_mock.return_value.Bug.get.return_value = {
            "bugs": [
                {
                    "summary": "summary",
                    "status": "status",
                    "is_open": False,
                    "resolution": "resolution",
                    "creation_time": datetime.datetime.fromtimestamp(0),
                    "last_change_time": datetime.datetime.fromtimestamp(1),
                    "creator_detail": {"real_name": "creator", "name": "creator"},
                    "assigned_to_detail": {"real_name": "assignee", "name": "assignee"},
                    "product": "product",
                    "component": "component",
                    "features": ["feature1", "feature2"],
                    "platforms": ["platform1", "platform2"],
                    "priority": "high",
                }
            ]
        }

        description = "Have you tried turning it off and on again?"
        ServerProxy_mock.return_value.Bug.comments.return_value = {
            "bugs": {
                "1": {
                    "comments": [
                        {
                            "text": description,
                            "creator": "Roy Trenneman",
                            "id": 100,
                            "count": 1,  # Wrong count number here
                            "time": datetime.datetime.fromtimestamp(0),
                            "creation_time": datetime.datetime.fromtimestamp(0)
                        }
                    ]
                }
            }
        }

        bug = MagicMock(spec=Bug, description=None)
        tracker = BugTracker.objects.create(tracker_type="bugzilla", features_field="features",
                                            platforms_field="platforms", public=True)

        with patch.object(BugComment.objects, "create"):
            with self.assertRaises(ValueError):
                Bugzilla(tracker).poll(bug)

        bug.save.assert_not_called()

    @patch('xmlrpc.client.ServerProxy')
    def test_search_bugs_ids__full(self, ServerProxy_mock):
        tracker = BugTracker.objects.create(tracker_type="bugzilla", public=True)
        bugzilla = Bugzilla(tracker)

        # Mock the return value of the search command
        ServerProxy_mock.return_value.Bug.search.return_value = {
            "bugs": [
                {"id": 10}, {"id": 11}, {"id": 13}
            ]
        }

        # Get the list of open bugs
        open_bugs = bugzilla.search_bugs_ids(components=["COMPONENT1", "COMPONENT2"],
                                             created_since=datetime.datetime.fromtimestamp(1000),
                                             status=['status1', 'status2'])
        self.assertEqual(open_bugs, set(['10', '11', '13']))

        # Verify that the request was valid
        expected_request = {
            "component": ["COMPONENT1", "COMPONENT2"],
            "status": ['status1', 'status2'],
            "creation_time": datetime.datetime.fromtimestamp(1000),
            "include_fields": ['id']
        }
        ServerProxy_mock.return_value.Bug.search.assert_called_with(expected_request)

    def test_open_statuses(self):
        self.assertEqual(Bugzilla.open_statuses, ["NEW", "ASSIGNED", "REOPENED", "NEEDINFO"])

    @patch('xmlrpc.client.ServerProxy')
    def test_search_bugs_ids__empty(self, ServerProxy_mock):
        tracker = BugTracker.objects.create(tracker_type="bugzilla", public=True)
        bugzilla = Bugzilla(tracker)

        bugzilla.search_bugs_ids()
        expected_request = {
            "include_fields": ['id']
        }
        ServerProxy_mock.return_value.Bug.search.assert_called_with(expected_request)

    @patch('xmlrpc.client.ServerProxy', BugzillaProxyMock)
    def test_auth_login(self):
        bugzilla = BugTracker.objects.create(tracker_type="bugzilla",
                                             url=BugzillaProxyMock.URL,
                                             username=BugzillaProxyMock.LOGIN,
                                             password=BugzillaProxyMock.PASSWORD,
                                             public=True)
        self.assertEqual(bugzilla.tracker.get_auth_token(), BugzillaProxyMock.TOKEN)

    @patch('xmlrpc.client.ServerProxy', BugzillaProxyMock)
    def test_auth_login__invalid_username(self):
        for username in [None, ""]:
            bugzilla = BugTracker(tracker_type="bugzilla",
                                  url=BugzillaProxyMock.URL,
                                  username=username,
                                  password=BugzillaProxyMock.PASSWORD,
                                  public=True)
            self.assertRaisesMessage(ValueError, "Invalid credentials",
                                     bugzilla.tracker.get_auth_token)

    @patch('xmlrpc.client.ServerProxy', BugzillaProxyMock)
    def test_auth_login__invalid_password(self):
        for password in [None, ""]:
            bugzilla = BugTracker(tracker_type="bugzilla",
                                  url=BugzillaProxyMock.URL,
                                  username=BugzillaProxyMock.LOGIN,
                                  password=password,
                                  public=True)
            self.assertRaisesMessage(ValueError, "Invalid credentials",
                                     bugzilla.tracker.get_auth_token)

    @patch('xmlrpc.client.ServerProxy', BugzillaProxyMock)
    def test_add_comment(self):
        bugzilla = BugTracker.objects.create(tracker_type="bugzilla",
                                             url=BugzillaProxyMock.URL,
                                             username=BugzillaProxyMock.LOGIN,
                                             password=BugzillaProxyMock.PASSWORD,
                                             public=True)
        bug = Bug(tracker=bugzilla, bug_id=str(BugzillaProxyMock.BUG_ID))
        bugzilla.tracker.add_comment(bug, BugzillaProxyMock.COMMENT)

    @patch('xmlrpc.client.ServerProxy', BugzillaProxyMock)
    @patch('CIResults.bugtrackers.Bugzilla.get_auth_token', return_value=None)
    def test_add_comment__invalid_credentials(self, auth_token_mocked):
        bugzilla = BugTracker.objects.create(tracker_type="bugzilla",
                                             url=BugzillaProxyMock.URL,
                                             username=BugzillaProxyMock.LOGIN,
                                             password=BugzillaProxyMock.PASSWORD,
                                             public=True)
        bug = Bug(tracker=bugzilla, bug_id=str(BugzillaProxyMock.BUG_ID))
        self.assertRaisesMessage(ValueError, "Authentication failed. Can't post a comment",
                                 bugzilla.tracker.add_comment, bug, BugzillaProxyMock.COMMENT)

    @patch('xmlrpc.client.ServerProxy')
    @patch('CIResults.bugtrackers.Bugzilla.get_auth_token', return_value=1234)
    def test_create_bug_from_json(self, auth_mock, ServerProxy_mock):
        tracker = BugTracker.objects.create(tracker_type="bugzilla", public=True, url="https://foo")
        bugzilla = Bugzilla(tracker)
        ServerProxy_mock.Bug.create = MagicMock(return_value={"id": 1})

        summary = "summary"
        description = "description"
        json_bug = {'summary': summary,
                    'description': description,
                    'labels': "Bug"}

        bugzilla.create_bug_from_json(json_bug)
        args = bugzilla._proxy.Bug.create.call_args[0][0]
        self.assertEqual(args['summary'], summary)
        self.assertEqual(args['description'], description)
        self.assertEqual(args['labels'], "Bug")

    @patch('xmlrpc.client.ServerProxy')
    @patch('CIResults.bugtrackers.Bugzilla.get_auth_token', return_value=1234)
    def test_create_bug_from_json_title(self, auth_mock, ServerProxy_mock):
        tracker = BugTracker.objects.create(tracker_type="bugzilla", public=True, url="https://foo")
        bugzilla = Bugzilla(tracker)
        ServerProxy_mock.Bug.create = MagicMock(return_value={"id": 1})

        summary = "summary"
        description = "description"
        json_bug = {'title': summary,
                    'description': description,
                    'labels': "Bug"}

        bugzilla.create_bug_from_json(json_bug)
        args = bugzilla._proxy.Bug.create.call_args[0][0]
        self.assertEqual(args['summary'], summary)
        self.assertEqual(args['description'], description)
        self.assertEqual(args['labels'], "Bug")

    @patch('xmlrpc.client.ServerProxy')
    @patch('CIResults.bugtrackers.Bugzilla.get_auth_token', return_value=1234)
    def test_create_bug_from_json_error(self, auth_mock, ServerProxy_mock):
        tracker = BugTracker.objects.create(tracker_type="bugzilla", public=True, url="https://foo")
        bugzilla = Bugzilla(tracker)
        bugzilla._proxy.Bug.create = MagicMock(side_effect=xmlrpc.client.Error())

        summary = "summary"
        description = "description"
        json_bug = {'title': summary,
                    'description': description,
                    'labels': "Bug"}

        with self.assertRaises(ValueError):
            bugzilla.create_bug_from_json(json_bug)

    @patch('xmlrpc.client.ServerProxy')
    @patch('CIResults.bugtrackers.Bugzilla.get_auth_token', return_value=None)
    def test_create_bug_from_json_invalid_token(self, auth_mock, ServerProxy_mock):
        tracker = BugTracker.objects.create(tracker_type="bugzilla", public=True, url="https://foo")
        bugzilla = Bugzilla(tracker)
        bugzilla._proxy.Bug.create = MagicMock(side_effect=xmlrpc.client.Error())

        summary = "summary"
        description = "description"
        json_bug = {'title': summary,
                    'description': description,
                    'labels': "Bug"}

        with self.assertRaises(ValueError):
            bugzilla.create_bug_from_json(json_bug)


class JiraMock:
    # New Bug
    NEW_BUG_ID = 5678
    PROJECT_KEY = "TEST"
    ISSUE_KEY = "TEST-101"
    SUMMARY = "This is a test bug"
    DESCRIPTION = "This is a description"

    ISSUE = MagicMock()
    ISSUE.key = ISSUE_KEY

    # URLs
    URL = "https://jira.instance.com/rest/api/2/issue"
    RESP_URL = urllib.parse.urljoin(URL, str(NEW_BUG_ID))
    REQ_URL = URL

    # User.login
    LOGIN = "userlogin"
    PASSWORD = "password"

    # Bug.add_comment
    BUG_ID = 1234
    COMMENT = 'my comment'

    # Bug.create_bug
    REQUEST_DATA = {"project": {
                            "key": PROJECT_KEY
                        },
                    "summary": SUMMARY,
                    "description": DESCRIPTION,
                    "issuetype": {
                            "name": "Bug"
                        }
                    }

    RESPONSES = {REQ_URL:
                 {"id": NEW_BUG_ID,
                  "key": ISSUE_KEY,
                  "self": RESP_URL}}


class BugTrackerJiraTests(TestCase):
    @patch('jira.JIRA.__init__', return_value=None)
    def test_jira__no_auth(self, JIRA_mocked):
        Jira(BugTracker(tracker_type="jira", url='https://jira.com', public=True)).jira
        JIRA_mocked.assert_called_with({'server': 'https://jira.com', 'verify': False})

    @patch('jira.JIRA.__init__', return_value=None)
    def test_jira__with_auth(self, JIRA_mocked):
        Jira(BugTracker(tracker_type="jira", url='https://jira.com', public=True,
                        username='user', password='password')).jira
        JIRA_mocked.assert_called_with({'server': 'https://jira.com', 'verify': False},
                                       basic_auth=('user', 'password'))

    @patch('CIResults.bugtrackers.Jira.jira')
    def test_poll(self, connection_mock):
        issue = MagicMock(fields=MagicMock(summary="summary", status=MagicMock(),
                                           description="description",
                                           created=datetime.datetime.fromtimestamp(0, tz=pytz.utc).isoformat(),
                                           updated=datetime.datetime.fromtimestamp(1, tz=pytz.utc).isoformat(),
                                           resolutiondate=datetime.datetime.fromtimestamp(42, tz=pytz.utc).isoformat(),
                                           creator=MagicMock(displayName="creator", key="creator_key"),
                                           assignee=MagicMock(displayName="assignee", key="assignee_key"),
                                           components=[MagicMock(), MagicMock()],
                                           comment=MagicMock(comments=[])))
        type(issue.fields.status).name = PropertyMock(return_value='status')
        type(issue.fields.priority).name = PropertyMock(return_value='Low')
        type(issue.fields.components[0]).name = PropertyMock(return_value='component1')
        type(issue.fields.components[1]).name = PropertyMock(return_value='component2')

        comment_created = datetime.datetime.fromtimestamp(47, tz=pytz.utc).isoformat()
        issue.fields.comment.comments.append(MagicMock(author=MagicMock(displayName='Last, First'),
                                                       id='12345', created=comment_created))
        type(issue.fields.comment.comments[0].author).name = PropertyMock(return_value='flast')
        issue.fields.comment.comments.append(MagicMock(author=MagicMock(displayName='Last, First2'),
                                                       id='12346', created=comment_created,))
        type(issue.fields.comment.comments[1].author).name = PropertyMock(return_value='flast2')

        connection_mock.issue.return_value = issue

        tracker = BugTracker.objects.create(tracker_type="jira", bug_base_url='https://jira.com/browse/',
                                            public=True)
        bug = Bug(bug_id='1234', tracker=tracker)
        bug._save = bug.save
        bug.save = MagicMock()
        Jira(tracker).poll(bug)

        self.assertEqual(bug.title, "summary")
        self.assertEqual(bug.created, datetime.datetime.fromtimestamp(0, tz=pytz.utc))
        self.assertEqual(bug.updated, datetime.datetime.fromtimestamp(1, tz=pytz.utc))
        self.assertEqual(bug.creator.person.full_name, "creator")
        self.assertEqual(bug.assignee.person.full_name, "assignee")
        self.assertEqual(bug.component, "component1,component2")
        self.assertEqual(bug.status, "status")
        self.assertEqual(bug.priority, "Low")
        self.assertEqual(bug.description, "description")
        self.assertEqual(bug.closed, datetime.datetime.fromtimestamp(42, tz=pytz.utc))

        # Check that the bug does not exist and no bugs have been polled
        bug.save.assert_not_called()
        self.assertEqual(len(BugComment.objects.all()), 0)

        # Save the bug, then poll again to check that the comments get created
        bug._save()
        bug.poll()
        bug.save.assert_not_called()

        # Check that the comments got created
        for c_id in ['12345', '12346']:
            comment = BugComment.objects.get(comment_id=c_id)
            self.assertEqual(comment.bug, bug)
            self.assertIn("flast", comment.account.user_id)
            self.assertIn("Last, First", comment.account.person.full_name)
            self.assertEqual(comment.url, "https://jira.com/browse/1234#comment-{}".format(c_id))
            self.assertEqual(comment.created_on, datetime.datetime.fromtimestamp(47, tz=pytz.utc))

    @patch('CIResults.bugtrackers.Jira.jira')
    def test_search_bugs_ids__full(self, connection_mock):
        tracker = BugTracker.objects.create(tracker_type="jira", project='PRODUCT',
                                            public=True)
        jira = Jira(tracker)

        # Mock the return value of the search command
        JiraBug = namedtuple('JiraBug', ('key', ))
        connection_mock.search_issues.return_value = [JiraBug(key="PRODUCT-10"),
                                                      JiraBug(key="PRODUCT-11"),
                                                      JiraBug(key="PRODUCT-13")]

        # Get the list of open bugs
        open_bugs = jira.search_bugs_ids(components=["COMPONENT1", "COMPONENT2"],
                                         created_since=datetime.datetime.fromtimestamp(125),
                                         status=['status1', 'status2'])
        self.assertEqual(open_bugs, set(["PRODUCT-10", "PRODUCT-11", "PRODUCT-13"]))

        # Verify that the request was valid
        connection_mock.search_issues.assert_called_with('issuetype = Bug AND project = \'PRODUCT\' '
                                                         'AND component in ("COMPONENT1", "COMPONENT2") '
                                                         'AND created > "1970/01/01 00:02" '
                                                         'AND status in ("status1", "status2")',
                                                         maxResults=False)

    def test_open_statuses(self):
        self.assertEqual(Jira.open_statuses, ['Open', 'In Progress'])

    @patch('CIResults.bugtrackers.Jira.jira')
    def test_add_comment(self, connection_mock):
        tracker = BugTracker.objects.create(tracker_type="jira", public=True)
        jira = Jira(tracker)

        issue = connection_mock.issue.return_value

        jira.add_comment(Bug(tracker=tracker, bug_id="JIRA-123"), "My comment")

        connection_mock.issue.assert_called_with("JIRA-123")
        connection_mock.add_comment.assert_called_with(issue, "My comment")

    @patch('CIResults.bugtrackers.Jira.jira', JiraMock)
    def test_create_bug_from_json(self):
        tracker = BugTracker.objects.create(tracker_type="jira", url="https://foo",
                                            project=JiraMock.PROJECT_KEY,
                                            public=True)
        j = Jira(tracker)
        resp = MagicMock(key=1)
        JiraMock.create_issue = MagicMock(return_value=resp)
        summary = "summary"
        description = "description"
        status = {"name": "FOO"}
        components = [{"name": "BAR"}, {"name": "Blah"}]
        json_bug = {'summary': summary,
                    'description': description,
                    'status': status,
                    'components': components,
                    'issue_type': {'name': "Bug"}}

        j.create_bug_from_json(json_bug)
        args, kwargs = JiraMock.create_issue.call_args_list[0]
        request = kwargs['fields']
        self.assertEqual(request['project'], {'key': JiraMock.PROJECT_KEY})
        del(request['project'])
        self.assertEqual(request['issuetype'], {'name': "Bug"})
        del(request['issuetype'])
        for field in json_bug:
            self.assertEqual(request[field], json_bug[field])

    @patch('CIResults.bugtrackers.Jira.jira', JiraMock)
    def test_create_bug_from_json_title(self):
        tracker = BugTracker.objects.create(tracker_type="jira", url="https://foo",
                                            project=JiraMock.PROJECT_KEY,
                                            public=True)
        j = Jira(tracker)
        resp = MagicMock(key=1)
        JiraMock.create_issue = MagicMock(return_value=resp)
        summary = "summary"
        description = "description"
        status = {"name": "FOO"}
        components = [{"name": "BAR"}, {"name": "Blah"}]
        json_bug = {'title': summary,
                    'description': description,
                    'status': status,
                    'components': components}

        j.create_bug_from_json(json_bug)
        args, kwargs = JiraMock.create_issue.call_args_list[0]
        request = kwargs['fields']
        self.assertEqual(request['summary'], summary)

    @patch('CIResults.bugtrackers.Jira.jira', JiraMock)
    def test_create_bug_from_json_issuetype(self):
        tracker = BugTracker.objects.create(tracker_type="jira", url="https://foo",
                                            project=JiraMock.PROJECT_KEY,
                                            public=True)
        j = Jira(tracker)
        resp = MagicMock(key=1)
        JiraMock.create_issue = MagicMock(return_value=resp)
        summary = "summary"
        description = "description"
        status = {"name": "FOO"}
        components = [{"name": "BAR"}, {"name": "Blah"}]
        json_bug = {'title': summary,
                    'description': description,
                    'status': status,
                    'components': components}

        j.create_bug_from_json(json_bug)
        args, kwargs = JiraMock.create_issue.call_args_list[0]
        request = kwargs['fields']
        self.assertEqual(request['issuetype'], {'name': "Bug"})

    @patch('CIResults.bugtrackers.Jira.jira', JiraMock)
    def test_create_bug_from_json_error(self):
        tracker = BugTracker.objects.create(tracker_type="jira", url="https://foo",
                                            project=JiraMock.PROJECT_KEY,
                                            public=True)
        j = Jira(tracker)
        JiraMock.create_issue = MagicMock(side_effect=JIRAError)
        summary = "summary"
        description = "description"
        status = {"name": "FOO"}
        components = [{"name": "BAR"}, {"name": "Blah"}]
        json_bug = {'title': summary,
                    'description': description,
                    'status': status,
                    'components': components}

        with self.assertRaises(ValueError):
            j.create_bug_from_json(json_bug)


class BugTrackerJiraUntrackedTests(TestCase):
    def setUp(self):
        self.db_tracker = BugTracker.objects.create(tracker_type="jira_untracked",
                                                    project='PROJECT', public=True)

    def test_poll(self):
        bug = MagicMock(spec=Bug)
        Untracked(self.db_tracker).poll(bug)

        self.assertEqual(bug.title, "UNKNOWN")
        self.assertEqual(bug.status, "UNKNOWN")
        bug.save.assert_not_called()

    def test_search_bugs_ids(self):
        self.assertEqual(Untracked(None).search_bugs_ids(), set())

    def test_open_statuses(self):
        self.assertEqual(Untracked(None).open_statuses, [])

    def test_add_comment(self):
        self.assertEqual(Untracked(None).add_comment(Bug(bug_id="1234"), "Hello World"), None)

    def test_create_bug(self):
        self.assertEqual(Untracked(self.db_tracker).create_bug(Bug(tracker=self.db_tracker)), None)
