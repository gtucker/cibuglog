# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-05-08 09:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CIResults', '0009_machine_add_tags_and_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='issuefilter',
            name='machine_tags',
            field=models.ManyToManyField(blank=True, help_text="The result's machine should have one of these tags (leave empty to ignore machines)", to='CIResults.MachineTag'),
        ),
        migrations.AlterField(
            model_name='issuefilter',
            name='machines',
            field=models.ManyToManyField(blank=True, help_text="The result's machine should be one of these machines (extends the set of machines selected by the machine tags, leave empty to ignore machines)", to='CIResults.Machine'),
        ),
    ]
