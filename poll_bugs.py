#!/usr/bin/env python3

from multiprocessing import Value
from multiprocessing.dummy import Pool
from datetime import timedelta
from django.utils import timezone

import django
import argparse
import traceback

django.setup()

from CIResults.models import BugTracker  # noqa


def poll_bug(bug):
    try:
        bug.poll(force_polling_comments=args.force_polling_comments)
        bug.save()
    except Exception:
        traceback.print_exc()

    with polled.get_lock():
        polled.value += 1
        print("{}/{}: polled {}".format(polled.value, len(bugs), bug.short_name))


# Parse the options
parser = argparse.ArgumentParser()
parser.add_argument("-f", "--force-polling-comments", action="store_true",
                    help="Re-download all the comments associated to bugs")
parser.add_argument("-d", '--delay', default=8640000, type=int,
                    help="Minimum delay (seconds) that needs to have passed before re-polling the bug. Default: 86400")
args = parser.parse_args()

bugtrackers = BugTracker.objects.all()

# Get the full list of bugs that are followed
bugs = set()
for bt in BugTracker.objects.all():
    followed_bugs = bt.followed_bugs()
    print("{}: Found {} bugs to follow".format(bt, len(followed_bugs)))

    # add to the list of bugs to poll
    bugs.update(followed_bugs)
print()

# Remove all the bugs that have been polled less than the specified delay
bugs = [bug for bug in bugs if bug.polled is None or timezone.now() - bug.polled > timedelta(seconds=args.delay)]

# Start polling in parallel (10 threads) to speed up the polling
print("Polling {} outdated bugs needing to be polled (older than {} seconds)".format(len(bugs), args.delay))
if len(bugs) > 0:
    polled = Value('i', 0)
    with Pool(processes=10) as pool:
        pool.map(poll_bug, bugs)
else:
    print("Found no bugs to poll. Did you set BugTracker.components_followed_since?")

# Update when the tracker was last polled
for bt in BugTracker.objects.all():
    bt.polled = timezone.now()
    bt.save()
